package HW1;

public class BankAccount{
	public double balance;
	
	public double withdraw(double amount) throws Exception {
		
		if(balance - amount < 0) throw new Exception("Not enough balance");
		balance -= amount;
		
		return balance;
		}
	
	public double deposit(double amount){
		balance += amount;
		
		return balance;
		}
	
	public static void main(String args[]) throws Exception{
		BankAccount ba = new BankAccount();
		ba.balance = 100.0;
		System.out.println(ba.balance);
		ba.deposit(100.0);
		System.out.println("deposit()");
		System.out.println(ba.balance);
		ba.deposit(-200.0);
		System.out.println("deposit()");
		System.out.println(ba.balance);
		ba.deposit(200.0);
		ba.deposit(200.0);
		System.out.println(ba.balance);
			ba.withdraw(-400.0);
		System.out.println(ba.balance);
		ba.deposit(-800);
		System.out.println(ba.balance);
			ba.withdraw(-400.0);
		System.out.println(ba.balance);
		ba.deposit(-500);
		System.out.println(ba.balance);
		ba.withdraw(-1000);
		System.out.println(ba.balance);
		}
	}