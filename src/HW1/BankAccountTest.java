package HW1;

import static org.junit.Assert.*;
import org.junit.Test;

public class BankAccountTest {

	@Test
	/** Tests BankAccount's withdraw function with a valid input
	* @param double amount
	* @return ending balance after amount has been subtracted from it. */
	public void test_valid_withdraw() throws Exception{

		//arrange
		BankAccount ba = new BankAccount();
		ba.balance = 100.0;
		double amount = 75.0;
		double expected_balance = ba.balance - amount; 
		
		//act
		ba.withdraw(amount);
		
		//assert
		assertTrue(ba.balance == expected_balance);
		
		//fail("Not yet implemented");
	}
	
	@Test
	/** Tests BankAccount's withdraw function with an invalid input
	* @param double amount
	* @return ending balance after amount has been subtracted from it. */
	public void test_invalid_withdraw() throws Exception{
		
		//arrange
		BankAccount ba = new BankAccount();
		ba.balance = 100.0;
		double too_much = 200.0;
		
		//act
		try{
			ba.withdraw(too_much);
			fail("Exception should have been thrown here!");
		}
		catch(Exception e){
			
			//assert
			assertEquals(e.getMessage(), "Not enough balance");
			
			//Error messages will be printed to console if you 
			//uncomment e.printStackTrace();
			//If the test passes with this line uncommented, it means it tripped an exception
			//but this exception is *explicitly expected* in that case.
			//Doesn't make sense to have a test passed, but with a bunch of red
			//output in the console, so leave it uncommented unless you're just curious.
			
			//e.printStackTrace(); 
		}
			
	}

	@Test
	/** Tests BankAccount's deposit function with a valid input
	* @param double amount
	* @return ending balance after amount has been added to it. */
	
	public void test_valid_deposit() {
		
		//arrange
		BankAccount ba = new BankAccount();
		ba.balance = 100.0;
		double amount = 75.0;
		double expected_balance = ba.balance + amount; 
		
		//act
		ba.deposit(amount);

		//assert
		assertTrue(ba.balance == expected_balance);
	}

}
