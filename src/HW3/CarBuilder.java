package HW3;

public class CarBuilder {
	private IGPS gps;
	private ITCE tce;
	private ICOLOR color;
	private ITTYPE ttype;
	private IMR mr;
	
	public CarBuilder setGPS(IGPS gps){
		this.gps = gps;
		return this;
	}
	public CarBuilder setTCE(ITCE tce){
		this.tce = tce;
		return this;
	}
	public CarBuilder setCOLOR(ICOLOR color){
		this.color = color;
		return this;
	}
	public CarBuilder setTTYPE(ITTYPE ttype){
		this.ttype = ttype;
		return this;
	}
	public CarBuilder setMR(IMR mr){
		this.mr = mr;
		return this;
	}
	
}
