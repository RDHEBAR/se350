package HW3;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.List;

public class StringCollectionComposition{
	final private List<String> l;
	public StringCollectionComposition(){l = new ArrayList<String>();}
	
	
	public void push(String s){
		if(s == null)throw new IllegalArgumentException();
		l.add(s);
		}
	
	public String pop(){
		if(l.isEmpty()) throw new EmptyStackException();
		return l.remove(l.size()-1);
		}
	
	
	public boolean contains (String s){
		Iterator<String> ite = l.iterator();
		while(ite.hasNext()){
			if(ite.next() == s)
				return true;
			}
		return false;
		}
						
}
