package HW3;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.Iterator;

class StringCollectionInheritance extends ArrayList<String>{
	
	//No idea what this is for, but the class on line 7 wanted this... Whatevs
	private static final long serialVersionUID = 1L;

	
	public void push(String s){
		if(s == null)throw new IllegalArgumentException();
		this.add(s);
		}
	
	public String pop(){
		if(this.isEmpty()) throw new EmptyStackException();
		return this.remove(this.size()-1);
		}
	
	
	public boolean contains (String s){
		Iterator<String> ite = this.iterator();
		while(ite.hasNext()){
			if(ite.next() == s)return true;
			}
		return false;
		}
	
}
