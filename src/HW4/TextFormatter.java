package HW4;
//Text formatting class utilizing the Strategy design pattern.
public class TextFormatter {
	private ITextFormatter formatStrat;
	public TextFormatter(ITextFormatter strat){
		formatStrat = strat;
	}
	public void printString(String input){
		input = formatStrat.printString(input);
		System.out.println(input);
	}
}
