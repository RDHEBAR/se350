package HW4;

public class UpperFormatter implements ITextFormatter {

	public String printString(String input) {
		input = input.replaceAll("\\+", "").toUpperCase();
		return input;
	}
}
