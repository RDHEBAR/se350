package Midterm;

import java.util.ArrayList;
import java.util.List;

 class Group implements Unit {
	private String _name; // Name of the group
	private List<Unit> _children; // People or sub-groups
	 
	Group(String name) {
		_name = name; 
		_children = new ArrayList<Unit>();
	}
	void add(Unit n) { _children.add(n); }
	public double getYComp() { /* To be implemented */return 0.0; }
	public String toString() { /* Implementation not important */return ""; }
	}