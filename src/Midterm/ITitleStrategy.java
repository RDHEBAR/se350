package Midterm;

public interface ITitleStrategy{

	String toString(String t);
	double getYComp(double base);
}
