package Midterm;

//Package specifier is above, but excluded as it shouldn't matter for your test driving.
//This test function assumes the Person constructor in Person.java is correctly outfitted with
//exception throwing measures upon incorrect inputs, which by default it is not.
//Without the exceptions in Person.java, these below test cases will hit the fail() statements.

import static org.junit.Assert.*;
import org.junit.Test;

public class PersonTest {

	@Test
	public void personConstructorRejectsInvalidTitle() {
	//title has to be one of three valid possibilities, 
	//or at least throw an IllegalArgumentException otherwise.
		//arrange9
		String nullWorkerType = null;
		String simpleEmptyStringWorkerType = ""; //I am only checking for "", not " ", or "\n" etc.
		String wrongWorkerType = "Intern";
		
		//act and assert
		
	  try{
		try{//Testing Person object instantiated with title of null
			Person nullWorker = new Person("Test", nullWorkerType, 1.0);
			fail("IllegalArgumentException should have caught this. Can't use null.");
			}catch(Exception e){
				System.out.println(e.getMessage());
				assertEquals(e.getMessage(), "Can't use null.");
			//assertTrue(e instanceof IllegalArgumentException);
			}
		
		try{//Testing Person object instantiated with title of ""
		Person emptyStringWorker = new Person("Test", simpleEmptyStringWorkerType, 1.0);
			fail("IllegalArgumentException should have caught this. Can't use simple empty string.");
			}catch(Exception e){
				System.out.println(e.getMessage());
				assertEquals(e.getMessage(), "Can't use simple empty string.");
			}
		
		try{//Testing Person object instantiated with title of "Intern"
			Person wrongWorker = new Person("Test", wrongWorkerType, 1.0);
			fail("IllegalArgumentException should have caught this. Unknown worker type.");
			}catch(Exception e){
				System.out.println(e.getMessage());
				assertEquals(e.getMessage(), "Unknown worker type.");
			}
      }catch(Exception d){
    	  fail("Something else went wrong.");
      	}
		
	}

}
