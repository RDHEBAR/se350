package Midterm;

/** A unit in an organization. */
public interface Unit {
	public double getYComp(); // Yearly compensation
	public String toString();
	}