package MovieTicketManagement;

public interface IMovie {
	int tickets = Movie._numTickets;
	float price = Movie._price;
	IMovieType nowSoldOut = Movie.SoldOutState; 
	
	String buyTickets(int quantity);
	boolean isOnSale();
	float getPrice();
}
