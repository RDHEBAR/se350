package MovieTicketManagement;

public interface IMovieSubscriber extends IMovieSubscriptionSubject{
	void update();
}