package MovieTicketManagement;

import java.util.List;

public interface IMovieSubscriptionSubject {
	IMovieType nowOnSale = Movie.OnSaleState; 
	List<IMovieSubscriber> subs = Movie._subs;
	void goOnSale();
	void notifyWhenOnSale(IMovieSubscriber subscriber);
}
