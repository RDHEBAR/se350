package MovieTicketManagement;

public interface IMovieType extends IMovie {
	public String buyTickets(int quantity);
	public boolean isOnSale();
	public float getPrice();
}
