package MovieTicketManagement;
import java.util.ArrayList;
import java.util.List;

//Subject = movies
//Observers = subscribers

public class Movie implements IMovie {
	
	static List<IMovieSubscriber> _subs;
	static float _price;
	static int _numTickets;
	static IMovieType _currentState;
//	static IMovieDecorator
	
	static final IMovieType OnSaleState = new OnSaleMovie();
	static final IMovieType NotOnSaleState = new NotOnSaleMovie();
	static final IMovieType SoldOutState = new SoldOutMovie();
	
	public Movie(float HowMuchItsGonnaSetYouBack, int tix){
		_subs = new ArrayList<IMovieSubscriber>();
		_price = HowMuchItsGonnaSetYouBack;
		_numTickets = tix;
		//_currentState = NotOnSaleState;
		_currentState = setType(NotOnSaleState);
		// Technically more correct since this uses the setter.
	}

	static IMovieType getType(){
		return _currentState;
	}

	
	static IMovieType setType(IMovieType newState){
		if(newState instanceof OnSaleMovie){
			_currentState = OnSaleState;
			//Alert now.
			}
		
		else if (newState instanceof NotOnSaleMovie){
			_currentState = NotOnSaleState;
			}
		
		else _currentState = SoldOutState;
		return _currentState;
	}
	
	@Override
	public String buyTickets(int quantity) {
		// TODO Auto-generated method stub
		return _currentState.buyTickets(quantity);
	}

	@Override
	public boolean isOnSale() {
		// TODO Auto-generated method stub
		return _currentState.isOnSale();
	}

	@Override
	public float getPrice() {
		// TODO Auto-generated method stub
		return _currentState.getPrice();
	}

}
