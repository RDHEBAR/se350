package MovieTicketManagement;

import static org.junit.Assert.*;

import org.junit.Test;

public class MovieTest {
	Movie m = new Movie((float)15.25, 300);
	Movie n = new Movie((float)12.25, 2000);
	Movie t = new Movie((float)15.25, 4000);
	static final IMovieType OnSaleState = new OnSaleMovie();
	static final IMovieType NotOnSaleState = new NotOnSaleMovie();
	static final IMovieType SoldOutState = new SoldOutMovie();
	
	
	
	@Test
	public void testMovie() {
		
		assertTrue(m._price > 0 && m._numTickets >0);
		
	}

	@Test
	public void testGetType() {
		assertEquals(m._currentState, m.getType());
	
	}

	@Test
	public void testSetType() {
		//Current state is NotOnSale.
		assertNotEquals(m._currentState, m.setType(SoldOutState));

	}

	@Test
	public void testBuyTickets() {
		m.setType(OnSaleState);
		assertEquals(m.buyTickets(400), "Not enough tickets left");
		
		
		
		t.setType(NotOnSaleState);
		assertEquals(t.buyTickets(1000), "Not yet on sale");
		
		
		n.setType(SoldOutState);
		assertEquals(n.buyTickets(2001), "Sold out");

	}

	@Test
	public void testIsOnSale() {
		assertFalse(m.isOnSale());
		n.setType(OnSaleState);
		assertTrue(n.isOnSale());

	}

	@Test
	public void testGetPrice() {
		
		assertTrue(n.getPrice() == 15.25);
		assertTrue(m.getPrice() == 15.25);

	}

}
