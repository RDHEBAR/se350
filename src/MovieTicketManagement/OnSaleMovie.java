package MovieTicketManagement;

public class OnSaleMovie implements IMovieType {

	@Override
	public String buyTickets(int quantity) {
		// TODO Auto-generated method stub
		//String ticketStatus = "Not enough tickets left";
		if(quantity - tickets >= 0){
			quantity = quantity - tickets;
			if(quantity == 0){
				Movie.setType(nowSoldOut);
			}
			return "Here are your tickets";
		}
		return "Not enough tickets left";
	}

	@Override
	public boolean isOnSale() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public float getPrice() {
		// TODO Auto-generated method stub
		return price;
	}
	
}
