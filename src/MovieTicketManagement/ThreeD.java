package MovieTicketManagement;

public class ThreeD implements IMovieDecorator {
	private IMovie movie;

	@Override
	public String buyTickets(int quantity) {
		// TODO Auto-generated method stub
		return movie.buyTickets(quantity);
	}

	@Override
	public boolean isOnSale() {
		// TODO Auto-generated method stub
		return movie.isOnSale();
	}

	@Override
	public float getPrice() {
		// TODO Auto-generated method stub
		return movie.getPrice()+3;
	}

}
