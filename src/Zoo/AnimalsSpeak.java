package Zoo;



public class AnimalsSpeak{
	
	public static IAnimal getAnimal(String animal){

		if(animal.equalsIgnoreCase("DOG")){
			new Dog().speak();
			}
		
		if(animal.equalsIgnoreCase("CAT")){
			new Cat().speak();
			}
		
		if(animal.equalsIgnoreCase("BEAR")){
			new Bear().speak();
			}
		
		if(animal.equalsIgnoreCase("COW")){
			new Cow().speak();
			}
		
		if(animal.equalsIgnoreCase("HORSE")){
			new Horse().speak();
		}
		return null;
	}

}
